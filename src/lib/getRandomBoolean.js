export function getRandomBoolean() {
    let output = !!randomBetweenInclusive(0, 1);
    return output;
};

function randomBetweenInclusive(min, max) {
    return Math.round(Math.random() * max + min);
};