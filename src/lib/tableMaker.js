import { elt } from './dom.js';
import {Matrix} from './matrix.js';


/**
 * Inputs Matrix, outputs table.
 * @param {Matrix} inputMatrix 
 * @returns { HTMLTableElement }
 */
export function tableFromMatrix (inputMatrix) {
    let output = elt('tbody');

    for (let row = 0; row < inputMatrix.height; row++) {
        const tableRowElt = elt('tr');
        for (let column = 0; column < inputMatrix.width; column++) {
            const tableDataElt = elt('td');
            let val = inputMatrix.getValue(row,column);
            tableDataElt['data-value'] = val;
            tableRowElt.appendChild(tableDataElt);
        };
        output.app.appendChild(tableRowElt);
    };

    return output;
};