/**
 * Where we create DOM nodes.
 */

 /**
  * A wrapper for 'document.createElement'
  * 
  * @param {String} tag name of the element
  * @param {Object} options To pass through to the API
  * @returns {HTMLElement}
  */
export function elt(tag, options) {
    let output = document.createElement(tag);

    if (options) {
        Object.keys(options).forEach(key => {
            output[key] = options[key];
        });
    };

    return output;
};