/**
 * Where we define a matrix for our game
 */

export class Matrix {
    constructor(height, width){
        this.height = height;
        this.width = width;
        this.grid = [...new Array(height * width)];
    };
    setValue (row, column, value) {
        this.grid[row * column] = value;
    };
    getValue(row, column) {
        return this.grid[row * column];
    };
    crawlMatrix (callback) {
        this.grid.forEach((value, index) => {
            callback(value, index);
        });
    };
    getAdjacent(row, column) {
        let output = [];
        output.push(this.getValue(row - 1, column - 1));
        output.push(this.getValue(row - 1, column - 0));
        output.push(this.getValue(row - 1, column + 1));
        
        output.push(this.getValue(row - 0, column - 1));
        output.push(this.getValue(row - 0, column + 1));
        
        output.push(this.getValue(row + 1, column - 1));
        output.push(this.getValue(row + 1, column - 0));
        output.push(this.getValue(row + 1, column + 1));
        return output;
    };
};