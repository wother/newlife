/**
 * Where we wrap HTML API for selectors.
 */

export function $$ (selector) {
    return document.querySelector(selector);
};

export function $$$ (selector) {
    return document.querySelectorAll(selector);
};