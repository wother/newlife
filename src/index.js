/**
 * Our Controller for the game.
 */

import { $$, $$$ } from './lib/selectors.js'
import { elt } from './lib/dom.js';
import { Matrix } from './lib/matrix.js'
import { getRandomBoolean } from './lib/getRandomBoolean.js';

$$('#startButton').addEventListener('click', function(evt){
    let heightValue = $$('#heightInput').value;
    let widthValue = $$('#widthInput').value;

    let gameBoard = new Matrix(heightValue, widthValue);

    gameBoard.crawlMatrix((value, index) => {
        gameBoard.grid[index] = getRandomBoolean();
    });

    // console.log("It has begun.");
    // console.group('Dimentions');
    // console.log(`Height: ${heightValue}`);
    // console.log(`Width: ${widthValue}`);
    // console.groupEnd();

    tableFromMatrix(gameBoard);

    console.log("Table Generated");
});

function tableFromMatrix (inputMatrix) {
    let tbody = elt('tbody', {id:"tableBody"});
    for (let rowIndex = 0; rowIndex < inputMatrix.height; rowIndex++) {
        let tableRow = elt('tr');
        for (let colIndex = 0; colIndex < inputMatrix.width; colIndex++) {
            let tdOpts = {
                innerHTML : inputMatrix.getValue(rowIndex, colIndex)
            };
            let tableData = elt('td', tdOpts);
            tableRow.appendChild(tableData);
        };
        tbody.appendChild(tableRow);
    };

    if ($$('#tableBody')) {
        $$('#gameBoardDiv').replaceChild(tbody, $$('#tableBody'))
    } else {
        $$('#gameBoardDiv').appendChild(tbody);
    }
};